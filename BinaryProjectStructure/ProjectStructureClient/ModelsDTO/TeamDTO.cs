﻿using System;

namespace ProjectStructureClient.ModelsDTO
{
    public class TeamDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
