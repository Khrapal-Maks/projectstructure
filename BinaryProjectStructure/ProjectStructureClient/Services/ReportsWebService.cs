﻿using Newtonsoft.Json;
using ProjectStructureClient.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ProjectStructureClient.Services
{
    public class ReportsWebService
    {
        private const string API_PATH = "https://localhost:44322/api/Reports";

        private HttpClient _client = new();

        public ReportsWebService()
        {
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<ProjectDTO>> GetAllProjects()
        {
            var response = await _client.GetAsync($"{API_PATH}/Projects");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<ProjectDTO>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<ProjectDTO> GetProject(int id)
        {
            var response = await _client.GetAsync($"{API_PATH}/Projects/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<ProjectDTO>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<TasksDTO>> GetAllTasks()
        {
            var response = await _client.GetAsync($"{API_PATH}/Tasks");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<TasksDTO>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<TasksDTO> GetTask(int id)
        {
            var response = await _client.GetAsync($"{API_PATH}/Tasks/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<TasksDTO>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<TeamDTO>> GetAllTeams()
        {
            var response = await _client.GetAsync($"{API_PATH}/Teams");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<TeamDTO>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<TeamDTO> GetTeam(int id)
        {
            var response = await _client.GetAsync($"{API_PATH}/Teams/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<TeamDTO>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<UserDTO>> GetAllUsers()
        {
            var response = await _client.GetAsync($"{API_PATH}/Users");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<UserDTO>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<UserDTO> GetlUser(int id)
        {
            var response = await _client.GetAsync($"{API_PATH}/Users/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<UserDTO>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Tuple<ProjectDTO, int>>> GetReportOne(int id)
        {
            var response = await _client.GetAsync($"{API_PATH}/GetAllTasksInProjectsByAuthor/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Tuple<ProjectDTO, int>>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<TasksDTO>> GetReportSecond(int id)
        {
            var response = await _client.GetAsync($"{API_PATH}/GetAllTasksOnPerformer/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<TasksDTO>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Tuple<int, string>>> GetReportThird(int id)
        {
            var response = await _client.GetAsync($"{API_PATH}/GetAllTasksThatFinished/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Tuple<int, string>>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Tuple<int, string, List<UserDTO>>>> GetReportFour()
        {
            var response = await _client.GetAsync($"{API_PATH}/GetAllUsersOldestThanTenYears");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Tuple<int, string, List<UserDTO>>>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Tuple<string, List<TasksDTO>>>> GetReportFive()
        {
            var response = await _client.GetAsync($"{API_PATH}/SortAllUsersFirstNameAndSortTaskOnName");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Tuple<string, List<TasksDTO>>>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<UserInfoDTO?> GetReportSix(int id)
        {
            var response = await _client.GetAsync($"{API_PATH}/GetStructUserById/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<UserInfoDTO?>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<ProjectInfoDTO>> GetReportSeven()
        {
            var response = await _client.GetAsync($"{API_PATH}/GetStructAllProjects");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<ProjectInfoDTO>>(await response.Content.ReadAsStringAsync());
        }
    }
}

