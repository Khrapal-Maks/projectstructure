﻿using Newtonsoft.Json;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.IO;

namespace ProjectStructure.DAL.Data
{
    public class VirtualDbContext : IVirtualDbContext
    {
        public VirtualDbContext()
        {
            GetProjects();
            GetTasks();
            GetTeams();
            GetUsers();
        }

        public List<Project> Projects { get; private set; }

        public List<Tasks> Tasks { get; private set; }

        public List<Team> Teams { get; private set; }

        public List<User> Users { get; private set; }

        private void GetProjects()
        {
            Projects = new List<Project>();

            Projects.AddRange(JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText(Path.GetFullPath("Data\\projects.json"))));
        }

        private void GetTasks()
        {
            Tasks = new List<Tasks>();

            Tasks.AddRange(JsonConvert.DeserializeObject<List<Tasks>>(File.ReadAllText(Path.GetFullPath("Data\\tasks.json"))));
        }

        private void GetTeams()
        {
            Teams = new List<Team>();

            Teams.AddRange(JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText(Path.GetFullPath("Data\\teams.json"))));
        }

        private void GetUsers()
        {
            Users = new List<User>();

            Users.AddRange(JsonConvert.DeserializeObject<List<User>>(File.ReadAllText(Path.GetFullPath("Data\\users.json"))));
        }
    }
}
