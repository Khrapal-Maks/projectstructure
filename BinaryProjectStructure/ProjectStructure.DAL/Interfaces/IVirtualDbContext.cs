﻿using ProjectStructure.DAL.Entities;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IVirtualDbContext
    {
        public List<Project> Projects { get; }

        public List<Tasks> Tasks { get; }

        public List<Team> Teams { get; }

        public List<User> Users { get; }
    }
}
