﻿using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Project> Projects { get; }

        IRepository<Tasks> Tasks { get; }

        IRepository<Team> Teams { get; }

        IRepository<User> Users { get; }
    }
}
