﻿using ProjectStructure.DAL.Data;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.DAL.Repositories
{
    public class DataUnitOfWork : IUnitOfWork
    {
        private IVirtualDbContext _db;

        private ProjectRepository _projectRepository;
        private TaskRepository _taskRepository;
        private TeamRepository _teamRepository;
        private UserRepository _userRepository;

        public DataUnitOfWork()
        {
            _db = new VirtualDbContext(); ;
        }

        public IRepository<Project> Projects
        {
            get
            {
                if (_projectRepository == null)
                    _projectRepository = new ProjectRepository(_db);
                return _projectRepository;
            }
        }

        public IRepository<Tasks> Tasks
        {
            get
            {
                if (_taskRepository == null)
                    _taskRepository = new TaskRepository(_db);
                return _taskRepository;
            }
        }
        public IRepository<Team> Teams
        {
            get
            {
                if (_teamRepository == null)
                    _teamRepository = new TeamRepository(_db);
                return _teamRepository;
            }
        }

        public IRepository<User> Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_db);
                return _userRepository;
            }
        }
    }
}
