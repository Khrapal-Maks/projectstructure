﻿using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.Repositories
{
    public class TaskRepository : IRepository<Tasks>
    {
        private IVirtualDbContext _db;

        public TaskRepository(IVirtualDbContext dbContext)
        {
            _db = dbContext;
        }

        public Tasks Create(Tasks task)
        {
            if (_db.Tasks.Select(x => x.Id).Contains(task.Id))
            {
                var newId = _db.Tasks.Max(x => x.Id) + 1;

                task.Id = newId;
            }

            _db.Tasks.Add(task);

            return task;
        }

        public void Delete(int id)
        {
            var deletedTask = _db.Tasks.Find(x => x.Id == id);
            if (deletedTask != null)
                _db.Tasks.Remove(deletedTask);
        }

        public IEnumerable<Tasks> Find(Func<Tasks, bool> predicate)
        {
            return _db.Tasks.Where(predicate).ToList();
        }

        public Tasks Get(int id)
        {
            return _db.Tasks.Find(x => x.Id == id);
        }

        public IEnumerable<Tasks> GetAll()
        {
            return _db.Tasks;
        }

        public void Update(Tasks tasks)
        {
            var updateTask = _db.Tasks.Find(x => x.Id == tasks.Id);

            updateTask.CreatedAt = tasks.CreatedAt;
            updateTask.FinishedAt = tasks.FinishedAt;
            updateTask.Description = tasks.Description;
            updateTask.Name = tasks.Name;
            updateTask.PerformerId = tasks.PerformerId;
            updateTask.ProjectId = tasks.ProjectId;
            updateTask.State = tasks.State;
        }
    }
}
