﻿using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private IVirtualDbContext _db;

        public TeamRepository(IVirtualDbContext dbContext)
        {
            _db = dbContext;
        }

        public Team Create(Team team)
        {
            if (_db.Teams.Select(x => x.Id).Contains(team.Id))
            {
                var newId = _db.Teams.Max(x => x.Id) + 1;

                team.Id = newId;
            }

            _db.Teams.Add(team);

            return team;
        }

        public void Delete(int id)
        {
            var deletedTeam = _db.Teams.Find(x => x.Id == id);
            if (deletedTeam != null)
                _db.Teams.Remove(deletedTeam);

            var teamInProject = _db.Projects.FindAll(x => x.TeamId == id);

            foreach (var project in teamInProject)
            {
                project.TeamId = 0;
            }

            var UsersInTeam = _db.Users.FindAll(x => x.TeamId == id);

            foreach (var user in UsersInTeam)
            {
                user.TeamId = 0;
            }
        }

        public IEnumerable<Team> Find(Func<Team, bool> predicate)
        {
            return _db.Teams.Where(predicate).ToList();
        }

        public Team Get(int id)
        {
            return _db.Teams.Find(x => x.Id == id);
        }

        public IEnumerable<Team> GetAll()
        {
            return _db.Teams;
        }

        public void Update(Team team)
        {
            var updateTeam = _db.Teams.Find(x => x.Id == team.Id);

            updateTeam.Name = team.Name;
            updateTeam.CreatedAt = team.CreatedAt;
        }
    }
}
