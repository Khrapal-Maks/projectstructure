﻿using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private IVirtualDbContext _db;

        public ProjectRepository(IVirtualDbContext dbContext)
        {
            _db = dbContext;
        }

        public Project Create(Project project)
        {
            if (_db.Projects.Select(x => x.Id).Contains(project.Id))
            {
                var newId = _db.Projects.Max(x => x.Id) + 1;

                project.Id = newId;
            }

            _db.Projects.Add(project);

            return project;
        }

        public void Delete(int id)
        {
            var deletedProject = _db.Projects.Find(x => x.Id == id);
            if (deletedProject != null)
                _db.Projects.Remove(deletedProject);

            _db.Tasks.RemoveAll(x => x.ProjectId == id);
        }

        public IEnumerable<Project> Find(Func<Project, bool> predicate)
        {
            return _db.Projects.Where(predicate).ToList();
        }

        public Project Get(int id)
        {
            return _db.Projects.Find(x => x.Id == id);
        }

        public IEnumerable<Project> GetAll()
        {
            return _db.Projects;
        }

        public void Update(Project project)
        {
            var updateProject = _db.Projects.Find(x => x.Id == project.Id);

            updateProject.AuthorId = project.AuthorId;
            updateProject.CreatedAt = project.CreatedAt;
            updateProject.Deadline = project.Deadline;
            updateProject.Description = project.Description;
            updateProject.Name = project.Name;
            updateProject.TeamId = project.TeamId;
        }
    }
}
