﻿using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private IVirtualDbContext _db;

        public UserRepository(IVirtualDbContext dbContext)
        {
            _db = dbContext;
        }

        public User Create(User user)
        {
            if (_db.Users.Select(x => x.Id).Contains(user.Id))
            {
                var newId = _db.Users.Max(x => x.Id) + 1;

                user.Id = newId;
            }

            _db.Users.Add(user);

            return user;
        }

        public void Delete(int id)
        {
            var deletedUser = _db.Users.Find(x => x.Id == id);
            if (deletedUser != null)
                _db.Users.Remove(deletedUser);

            var userInProjects = _db.Projects.FindAll(x => x.AuthorId == id);

            foreach (var project in userInProjects)
            {
                project.AuthorId = 0;
            }

            var userInTasks = _db.Tasks.FindAll(x => x.PerformerId == id);

            foreach (var task in userInTasks)
            {
                task.PerformerId = 0;
            }
        }

        public IEnumerable<User> Find(Func<User, bool> predicate)
        {
            return _db.Users.Where(predicate).ToList();
        }

        public User Get(int id)
        {
            return _db.Users.Find(x => x.Id == id);
        }

        public IEnumerable<User> GetAll()
        {
            return _db.Users;
        }

        public void Update(User user)
        {
            var updateUser = _db.Users.Find(x => x.Id == user.Id);

            updateUser.BirthDay = user.BirthDay;
            updateUser.Email = user.Email;
            updateUser.FirstName = user.FirstName;
            updateUser.LastName = user.LastName;
            updateUser.RegisteredAt = user.RegisteredAt;
            updateUser.TeamId = user.TeamId;
        }
    }
}
