﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public sealed class TasksService : BaseService, ITasksService
    {
        IUnitOfWork Database { get; set; }

        public TasksService(IUnitOfWork context, IMapper mapper) : base(mapper)
        {
            Database = context;
        }

        public IEnumerable<TasksDTO> GetAllTasks()
        {
            var tasks = Database.Tasks.GetAll().ToList();

            return _mapper.Map<IEnumerable<TasksDTO>>(tasks);
        }

        public TasksDTO GetTask(int id)
        {
            var task = Database.Tasks.Get(id);

            return _mapper.Map<TasksDTO>(task);
        }

        public TasksDTO CreateTask(NewTaskDTO newTask)
        {
            var taskEntity = _mapper.Map<Tasks>(newTask);

            var createdTask = Database.Tasks.Create(taskEntity);

            return _mapper.Map<TasksDTO>(createdTask);
        }

        public TasksDTO UpdateTask(TasksDTO task)
        {
            var updateTask = _mapper.Map<Tasks>(task);

            Database.Tasks.Update(updateTask);

            var updatedTask = Database.Tasks.Get(task.Id);

            return _mapper.Map<TasksDTO>(updatedTask);
        }

        public void DeleteTask(int id)
        {
            Database.Tasks.Delete(id);
        }
    }
}
