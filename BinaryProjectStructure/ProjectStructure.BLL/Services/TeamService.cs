﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public class TeamService : BaseService, ITeamService
    {
        IUnitOfWork Database { get; set; }

        public TeamService(IUnitOfWork context, IMapper mapper) : base(mapper)
        {
            Database = context;
        }

        public IEnumerable<TeamDTO> GetAllTeams()
        {
            var teams = Database.Teams.GetAll().ToList();

            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }

        public TeamDTO GetTeam(int id)
        {
            var team = Database.Teams.Get(id);

            return _mapper.Map<TeamDTO>(team);
        }

        public TeamDTO CreateTeam(NewTeamDTO newTeam)
        {
            var teamEntity = _mapper.Map<Team>(newTeam);

            var createdTeam = Database.Teams.Create(teamEntity);

            return _mapper.Map<TeamDTO>(createdTeam);
        }



        public TeamDTO UpdateTeam(TeamDTO team)
        {
            var updateTeam = _mapper.Map<Team>(team);

            Database.Teams.Update(updateTeam);

            var updatedTeam = Database.Teams.Get(team.Id);

            return _mapper.Map<TeamDTO>(updatedTeam);
        }

        public void DeleteTeam(int id)
        {
            Database.Teams.Delete(id);
        }
    }
}
