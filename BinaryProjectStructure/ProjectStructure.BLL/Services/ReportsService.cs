﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public sealed class ReportsService : BaseService, IReportsService
    {
        IUnitOfWork Database { get; set; }

        public ReportsService(IUnitOfWork context, IMapper mapper) : base(mapper)
        {
            Database = context;
        }

        public List<Tuple<ProjectDTO, int>> GetAllTasksInProjectsByAuthor(int authorId)
        {
            return Database.Projects.GetAll().Where(x => x.AuthorId == authorId)
                .GroupJoin(Database.Tasks.GetAll(),
                projects => projects.Id,
                tasks => tasks.ProjectId,
                (pr, ts) => new { Projects = pr, Tasks = ts })
                .Select(x => Tuple.Create(new ProjectDTO
                {
                    Id = x.Projects.Id,
                    AuthorId = x.Projects.AuthorId,
                    CreatedAt = x.Projects.CreatedAt,
                    Deadline = x.Projects.Deadline,
                    Description = x.Projects.Description,
                    Name = x.Projects.Name,
                    TeamId = x.Projects.TeamId
                }, x.Tasks.Count())).ToList();
        }

        public List<TasksDTO> GetAllTasksOnPerformer(int performerId)
        {
            int lengthNameTask = 45;

            var result = Database.Tasks.GetAll().Where(x => x.PerformerId == performerId & x.Name.Length < lengthNameTask).ToList();

            return _mapper.Map<IEnumerable<TasksDTO>>(result).ToList();
        }

        public List<Tuple<int, string>> GetAllTasksThatFinished(int performerId)
        {
            int year = 2021;

            return Database.Tasks.GetAll()
                   .Where(x => x.PerformerId == performerId)
                   .Where(x => x.FinishedAt.HasValue && x.FinishedAt.Value.Year == year)
                   .Select(x => Tuple.Create(x.Id, x.Name)).ToList();
        }

        public List<Tuple<int, string, List<UserDTO>>> GetAllUsersOldestThanTenYears()
        {
            int ageUser = 10;

            return Database.Teams.GetAll()
                .GroupJoin(Database.Users.GetAll()
                .Where(x => x.BirthDay.AddYears(ageUser) < DateTime.Now)
                .OrderByDescending(x => x.RegisteredAt),
                team => team.Id,
                performer => performer.TeamId,
                (tm, pr) => new
                {
                    Team = tm,
                    Users = pr
                }).Select(x => Tuple.Create(x.Team, _mapper.Map<IEnumerable<UserDTO>>(x.Users).ToList())).Distinct()
                .Select(x => Tuple.Create(x.Item1.Id, x.Item1.Name, x.Item2.ToList())).ToList();
        }

        public List<Tuple<string, List<TasksDTO>>> SortAllUsersFirstNameAndSortTaskOnName()
        {
            return Database.Users.GetAll()
                .OrderBy(x => x.FirstName)
                .GroupJoin(Database.Tasks.GetAll().OrderByDescending(x => x.Name.Length),
                performer => performer.Id,
                task => task.PerformerId,
                (pr, ts) => new { Performer = pr, Tasks = ts })
                .Select(x => Tuple.Create(x.Performer.FirstName, _mapper.Map<IEnumerable<TasksDTO>>(x.Tasks).ToList()))
                .Where(x => x.Item2.Any()).ToList();
        }

        public UserInfo? GetStructUserById(int id)
        {
            var date = new DateTime(0001, 01, 01, 00, 00, 00);

            var user = Database.Users.Get(id) ?? null;

            if (user == null)
            {
                return null;
            }

            var project = Database.Projects.Find(x => x.AuthorId == user.Id).OrderByDescending(x => x.CreatedAt).FirstOrDefault() ?? null;

            if (project == null)
            {
                return null;
            }

            var countTasksInProject = Database.Tasks.Find(x => x.ProjectId == project.Id).Where(x => x.CreatedAt != date).Count();

            var countTasksInWork = Database.Tasks.GetAll().Count(x => x.PerformerId == user.Id & x.FinishedAt != date);

            var task = Database.Tasks.Find(x => x.PerformerId == user.Id).OrderByDescending(x => x.FinishedAt - x.CreatedAt).FirstOrDefault() ?? null;

            return new UserInfo
            {
                User = user,
                Project = project,
                CountTasksInProject = countTasksInProject,
                CountTasksInWork = countTasksInWork,
                Tasks = task
            };
        }

        public List<ProjectInfo> GetStructAllProjects()
        {
            int descriptionLength = 20;
            int countTask = 3;

            return Database.Projects.GetAll()
                .GroupJoin(Database.Tasks.GetAll(),
                project => project.Id,
                task => task.ProjectId,
                (project, task) => new { Projects = project, Tasks = task })
                .Join(Database.Teams.GetAll(),
                project => project.Projects.TeamId,
                team => team.Id,
                (project, team) => new { Project = project, Team = team })
                .GroupJoin(Database.Users.GetAll(),
                project => project.Team.Id,
                user => user.TeamId,
                (project, user) => new ProjectInfo
                {
                    Project = project.Project.Projects,
                    TaskLongDescription = project.Project.Tasks.OrderBy(x => x.Description.Length).FirstOrDefault() ?? new Tasks(),
                    TasksShortName = project.Project.Tasks.OrderBy(x => x.Name.Length).FirstOrDefault() ?? new Tasks(),
                    CountUsersInTeamProject =
                    project.Project.Projects.Description.Length > descriptionLength | project.Project.Tasks.Count() < countTask
                    ? user.Count() : 0
                }).Distinct().ToList();
        }
    }
}
