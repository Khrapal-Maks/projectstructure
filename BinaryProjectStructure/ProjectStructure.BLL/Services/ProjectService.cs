﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public sealed class ProjectService : BaseService, IProjectService
    {
        IUnitOfWork Database { get; set; }

        public ProjectService(IUnitOfWork context, IMapper mapper) : base(mapper)
        {
            Database = context;
        }

        public IEnumerable<ProjectDTO> GetAllProjects()
        {
            var projects = Database.Projects.GetAll().ToList();

            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }

        public ProjectDTO GetProject(int id)
        {
            var project = Database.Projects.Get(id);

            return _mapper.Map<ProjectDTO>(project);
        }

        public ProjectDTO CreateProject(NewProjectDTO project)
        {
            var projectEntity = _mapper.Map<Project>(project);

            var createdProject = Database.Projects.Create(projectEntity);

            return _mapper.Map<ProjectDTO>(createdProject);
        }

        public ProjectDTO UpdateProject(ProjectDTO project)
        {
            var updateProject = _mapper.Map<Project>(project);

            Database.Projects.Update(updateProject);

            var updatedProject = Database.Projects.Get(project.Id);

            return _mapper.Map<ProjectDTO>(updatedProject);
        }

        public void DeleteProject(int id)
        {
            Database.Projects.Delete(id);
        }
    }
}
