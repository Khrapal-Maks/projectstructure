﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Services
{
    public sealed class UsersService : BaseService, IUsersService
    {
        IUnitOfWork Database { get; set; }

        public UsersService(IUnitOfWork context, IMapper mapper) : base(mapper)
        {
            Database = context;
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            var users = Database.Users.GetAll().ToList();

            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }

        public UserDTO GetUser(int id)
        {
            var user = Database.Users.Get(id);

            return _mapper.Map<UserDTO>(user);
        }

        public UserDTO CreateUser(NewUserDTO newUser)
        {
            var userEntity = _mapper.Map<User>(newUser);

            var createdUser = Database.Users.Create(userEntity);

            return _mapper.Map<UserDTO>(createdUser);
        }

        public UserDTO UpdateUser(UserDTO user)
        {
            var updateUser = _mapper.Map<User>(user);

            Database.Users.Update(updateUser);

            var updatedUser = Database.Users.Get(user.Id);

            return _mapper.Map<UserDTO>(updatedUser);
        }

        public void DeleteUser(int id)
        {
            Database.Users.Delete(id);
        }
    }
}
