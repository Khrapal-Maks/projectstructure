﻿using ProjectStructure.BLL.ModelsDTO;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Services
{
    public interface IProjectService
    {
        IEnumerable<ProjectDTO> GetAllProjects();

        ProjectDTO CreateProject(NewProjectDTO item);

        ProjectDTO GetProject(int value);

        ProjectDTO UpdateProject(ProjectDTO item);

        void DeleteProject(int value);
    }
}
