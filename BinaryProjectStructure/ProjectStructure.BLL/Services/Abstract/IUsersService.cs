﻿using ProjectStructure.BLL.ModelsDTO;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Services
{
    public interface IUsersService
    {
        IEnumerable<UserDTO> GetAllUsers();

        UserDTO CreateUser(NewUserDTO item);

        UserDTO GetUser(int value);

        UserDTO UpdateUser(UserDTO item);

        void DeleteUser(int value);
    }
}
