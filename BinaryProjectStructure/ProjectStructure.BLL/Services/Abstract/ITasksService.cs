﻿using ProjectStructure.BLL.ModelsDTO;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Services
{
    public interface ITasksService
    {
        IEnumerable<TasksDTO> GetAllTasks();

        TasksDTO CreateTask(NewTaskDTO item);

        TasksDTO GetTask(int value);

        TasksDTO UpdateTask(TasksDTO item);

        void DeleteTask(int value);
    }
}
