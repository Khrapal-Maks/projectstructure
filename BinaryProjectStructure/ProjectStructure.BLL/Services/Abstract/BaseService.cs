﻿using AutoMapper;

namespace ProjectStructure.BLL.Services
{
    public abstract class BaseService
    {
        private protected readonly IMapper _mapper;

        public BaseService(IMapper mapper)
        {
            _mapper = mapper;
        }
    }
}
