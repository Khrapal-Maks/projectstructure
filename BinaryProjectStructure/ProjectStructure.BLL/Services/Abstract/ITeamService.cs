﻿using ProjectStructure.BLL.ModelsDTO;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Services
{
    public interface ITeamService
    {
        IEnumerable<TeamDTO> GetAllTeams();

        TeamDTO CreateTeam(NewTeamDTO item);

        TeamDTO GetTeam(int value);

        TeamDTO UpdateTeam(TeamDTO item);

        void DeleteTeam(int value);
    }
}
