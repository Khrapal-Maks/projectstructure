﻿using ProjectStructure.BLL.ModelsDTO;
using System;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Services
{
    public interface IReportsService
    {
        List<Tuple<ProjectDTO, int>> GetAllTasksInProjectsByAuthor(int value);

        List<TasksDTO> GetAllTasksOnPerformer(int value);

        List<Tuple<int, string>> GetAllTasksThatFinished(int value);

        List<Tuple<int, string, List<UserDTO>>> GetAllUsersOldestThanTenYears();

        List<Tuple<string, List<TasksDTO>>> SortAllUsersFirstNameAndSortTaskOnName();

        UserInfo? GetStructUserById(int value);

        List<ProjectInfo> GetStructAllProjects();
    }
}
