﻿using System;

namespace ProjectStructure.BLL.ModelsDTO
{
    public sealed class NewTaskDTO
    {
        public int ProjectId { get; set; }

        public int PerformerId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public TaskStateDTO State { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? FinishedAt { get; set; }
    }
}
