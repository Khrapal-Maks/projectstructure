﻿using System;

namespace ProjectStructure.BLL.ModelsDTO
{
    public sealed class NewTeamDTO
    {
        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
