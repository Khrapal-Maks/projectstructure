﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ProjectStructure.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        private readonly IReportsService _reportsService;
        private readonly ILogger<Project> _logger;

        public ReportsController(IReportsService reportsService, ILogger<Project> logger)
        {
            _reportsService = reportsService;
            _logger = logger;
        }

        [HttpGet("[action]/{id}")]
        public ActionResult<List<Tuple<ProjectDTO, int>>> GetAllTasksInProjectsByAuthor(int id)
        {
            var result = _reportsService.GetAllTasksInProjectsByAuthor(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("[action]/{id}")]
        public ActionResult<List<TasksDTO>> GetAllTasksOnPerformer(int id)
        {
            var result = _reportsService.GetAllTasksOnPerformer(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("[action]/{id}")]
        public ActionResult<List<Tuple<int, string>>> GetAllTasksThatFinished(int id)
        {
            var result = _reportsService.GetAllTasksThatFinished(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("[action]")]
        public ActionResult<List<Tuple<int, string, List<UserDTO>>>> GetAllUsersOldestThanTenYears()
        {
            var result = _reportsService.GetAllUsersOldestThanTenYears();

            if (result == null)
            {
                return NoContent();
            }

            return Ok(result);
        }

        [HttpGet("[action]")]
        public ActionResult<List<Tuple<string, List<TasksDTO>>>> SortAllUsersFirstNameAndSortTaskOnName()
        {
            var result = _reportsService.SortAllUsersFirstNameAndSortTaskOnName();

            if (result == null)
            {
                return NoContent();
            }

            return Ok(result);
        }

        [HttpGet("[action]/{id}")]
        public ActionResult<UserInfo> GetStructUserById(int id)
        {
            var result = _reportsService.GetStructUserById(id);

            return Ok(result);
        }

        [HttpGet("[action]")]
        public ActionResult<List<ProjectInfo>> GetStructAllProjects()
        {
            var result = _reportsService.GetStructAllProjects();

            return Ok(result);
        }
    }
}
