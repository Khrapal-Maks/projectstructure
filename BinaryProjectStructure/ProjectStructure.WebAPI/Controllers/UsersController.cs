﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Entities;
using System.Collections.Generic;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;
        private readonly ILogger<User> _logger;

        public UsersController(IUsersService usersService, ILogger<User> logger)
        {
            _usersService = usersService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> GetAllUsers()
        {
            return Ok(_usersService.GetAllUsers());
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<UserDTO>> GetUser(int id)
        {
            var user = _usersService.GetUser(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpPost]
        public ActionResult<UserDTO> CreateUser([FromBody] NewUserDTO newUser)
        {
            return Ok(_usersService.CreateUser(newUser));
        }

        [HttpPut("{id}")]
        public ActionResult<UserDTO> UpdateUser(int id, [FromBody] UserDTO user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            var userToUpdate = _usersService.GetUser(id);

            if (userToUpdate == null)
            {
                return NotFound();
            }

            return Ok(_usersService.UpdateUser(user));
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteUser(int id)
        {
            if (id < 0)
            {
                return BadRequest();
            }

            var userToDelete = _usersService.GetUser(id);

            if (userToDelete == null)
            {
                return NotFound();
            }

            _usersService.DeleteUser(id);
            return Ok();
        }
    }
}
