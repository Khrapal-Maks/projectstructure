﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Entities;
using System.Collections.Generic;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;
        private readonly ILogger<Project> _logger;

        public ProjectsController(IProjectService projectService, ILogger<Project> logger)
        {
            _projectService = projectService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> GetAllProjects()
        {
            return Ok(_projectService.GetAllProjects());
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<ProjectDTO>> GetProject(int id)
        {
            var project = _projectService.GetProject(id);

            if (project == null)
            {
                return NotFound();
            }

            return Ok(project);
        }

        [HttpPost]
        public ActionResult<ProjectDTO> CreateProject([FromBody] NewProjectDTO newProject)
        {
            return Ok(_projectService.CreateProject(newProject));
        }

        [HttpPut("{id}")]
        public ActionResult<ProjectDTO> UpdateProject(int id, [FromBody] ProjectDTO project)
        {
            if (id != project.Id)
            {
                return BadRequest();
            }

            var projectToUpdate = _projectService.GetProject(id);

            if (projectToUpdate == null)
            {
                return NotFound();
            }

            return Ok(_projectService.UpdateProject(project));
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteProject(int id)
        {
            if (id < 0)
            {
                return BadRequest();
            }

            var projectToDelete = _projectService.GetProject(id);

            if (projectToDelete == null)
            {
                return NotFound();
            }

            _projectService.DeleteProject(id);
            return Ok();
        }
    }
}
