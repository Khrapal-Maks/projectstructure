﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Entities;
using System.Collections.Generic;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;
        private readonly ILogger<Team> _logger;

        public TeamController(ITeamService teamService, ILogger<Team> logger)
        {
            _teamService = teamService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> GetAllTeams()
        {
            return Ok(_teamService.GetAllTeams());
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<TeamDTO>> GetTeam(int id)
        {
            var team = _teamService.GetTeam(id);

            if (team == null)
            {
                return NotFound();
            }

            return Ok(team);
        }

        [HttpPost]
        public ActionResult<TeamDTO> CreateTeam([FromBody] NewTeamDTO newTeam)
        {
            return Ok(_teamService.CreateTeam(newTeam));
        }

        [HttpPut("{id}")]
        public ActionResult<TeamDTO> UpdateTeam(int id, [FromBody] TeamDTO team)
        {
            if (id != team.Id)
            {
                return BadRequest();
            }

            var teamToUpdate = _teamService.GetTeam(id);

            if (teamToUpdate == null)
            {
                return NotFound();
            }

            return Ok(_teamService.UpdateTeam(team));
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteTeam(int id)
        {
            if (id < 0)
            {
                return BadRequest();
            }

            var teamToDelete = _teamService.GetTeam(id);

            if (teamToDelete == null)
            {
                return NotFound();
            }

            _teamService.DeleteTeam(id);
            return Ok();
        }
    }
}
