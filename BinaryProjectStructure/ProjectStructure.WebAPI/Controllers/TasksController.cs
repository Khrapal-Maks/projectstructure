﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Entities;
using System.Collections.Generic;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITasksService _tasksService;
        private readonly ILogger<Tasks> _logger;

        public TasksController(ITasksService tasksService, ILogger<Tasks> logger)
        {
            _tasksService = tasksService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TasksDTO>> GetAllTasks()
        {
            return Ok(_tasksService.GetAllTasks());
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<TasksDTO>> GetTask(int id)
        {
            var task = _tasksService.GetTask(id);

            if (task == null)
            {
                return NotFound();
            }

            return Ok(task);
        }

        [HttpPost]
        public ActionResult<TasksDTO> CreateTask([FromBody] NewTaskDTO newTask)
        {
            return Ok(_tasksService.CreateTask(newTask));
        }

        [HttpPut("{id}")]
        public ActionResult<TasksDTO> UpdateTask(int id, [FromBody] TasksDTO task)
        {
            if (id != task.Id)
            {
                return BadRequest();
            }

            var taskToUpdate = _tasksService.GetTask(id);

            if (taskToUpdate == null)
            {
                return NotFound();
            }

            return Ok(_tasksService.UpdateTask(task));
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteTask(int id)
        {
            if (id < 0)
            {
                return BadRequest();
            }

            var taskToDelete = _tasksService.GetTask(id);

            if (taskToDelete == null)
            {
                return NotFound();
            }

            _tasksService.DeleteTask(id);
            return Ok();
        }
    }
}
