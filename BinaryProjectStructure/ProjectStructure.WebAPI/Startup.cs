using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Repositories;

namespace ProjectStructure.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IMapper mapper = GetMapper();
            IUnitOfWork _uow = new DataUnitOfWork();

            services.AddControllers();
            services.AddSingleton(mapper);

            services.AddSingleton<IUnitOfWork, DataUnitOfWork>();
            services.AddScoped<IProjectService, ProjectService>(ps => new ProjectService(_uow, mapper));
            services.AddScoped<ITasksService, TasksService>(ts => new TasksService(_uow, mapper));
            services.AddScoped<ITeamService, TeamService>(ts => new TeamService(_uow, mapper));
            services.AddScoped<IUsersService, UsersService>(us => new UsersService(_uow, mapper));
            services.AddScoped<IReportsService, ReportsService>(rs => new ReportsService(_uow, mapper));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ProjectStructure.WebAPI", Version = "v1" });
            });
        }

        private static IMapper GetMapper()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ProjectProfile());
                mc.AddProfile(new TaskProfile());
                mc.AddProfile(new TeamProfile());
                mc.AddProfile(new UserProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            return mapper;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProjectStructure.WebAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
